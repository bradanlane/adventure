# SRXE Colossal Cave

The classic 1970's text adventure game, Colossal Cave (aka Adventure), running on the SMART Response XE device

## Original Source Material

The original source material is a long and winding path (most likely down a hill and near a well house). This project started from the 
[Don Wood 430 version](https://github.com/Quuxplusone/Advent/tree/master/WOOD0430).

Don Wood included the following copyright, which is reproduced here for completeness.

>>>
Don Wood's Adventure 2.5: (c) Copyright 1995 by Donald R. Woods.

This software may be freely redistributed if this notice is retained.
(The author apologises for the style of the code; it is a result of
running the original Fortran IV source through a home-brew Fortran-to-C
converter.)
>>>

## Design _(plan)_

The SMART Resonse XE (SRXE) is a comercial hand help device originally designed for the classroom. It has a chicklet keyboard, LCD display, and 2.4GHz transceiver. It uses an Atmel ATMega128RFA1. It also has a 128KB flash chip.

To get as much or the orignal Colossal Cave content onto the SRXE, the story data is stored on the flash chip while the program resides on the ATMega128RFA1.

The plan is to have two programs. The first is a minimal 'utility' which contains all of the story data and just enough code to copy teh story to the flach chip. The second program provides the game play and the SRXEcore services to take advantage of the SRXE hardware.
